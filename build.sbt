name := """tutorial"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

lazy val mysqlVersion = "5.1.33"
lazy val scalikejdbcVersion = "2.2.3"
lazy val scalikejdbcPluginVersion = "2.3.2"

scalaVersion := "2.11.1"

resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"

resolvers += "Sonatype OSS Snapshot Repository" at "https://oss.sonatype.org/content/repositories/snapshots/"

resolvers += "Sonatype OSS Release Repository" at "https://oss.sonatype.org/content/repositories/releases/"


libraryDependencies ++= Seq(
  "org.sisioh" %% "scala-dddbase-core" % "0.2.0-SNAPSHOT",
  "org.sisioh" %% "scala-dddbase-lifecycle-repositories-memory" % "0.2.0-SNAPSHOT",
  "mysql" % "mysql-connector-java" % mysqlVersion,
  "org.scalikejdbc" %% "scalikejdbc" % scalikejdbcVersion,
  "org.scalikejdbc" %% "scalikejdbc-config" % scalikejdbcVersion,
  "org.scalikejdbc" %% "scalikejdbc-interpolation" % scalikejdbcVersion,
  "org.scalikejdbc" %% "scalikejdbc-test" % scalikejdbcVersion % "test",
  jdbc,
  anorm,
  cache,
  ws
)

seq(flywaySettings: _*)

flywayUrl := "jdbc:mysql://localhost:3306/board"

flywayUser := "root"

flywayPassword := "nekoneko+0511"