package controllers

import domain.lifecycle.SyncEntityIOContextOnJDBC
import domain.lifecycle.user.UserRepository
import domain.model.user.{UserId, User}
import org.sisioh.dddbase.core.lifecycle.EntityNotFoundException
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.openid._
import play.api.mvc._

import scala.collection._

object OpenIdController extends Controller {

  def authenticate = Action.async { implicit request =>
    OpenID.redirectURL(
      "www.google.com/accounts/o8/id",
      routes.OpenIdController.openIDCallback.absoluteURL(),
      Seq(
        "email" -> "http://axschema.org/contact/email",
        "firstname" -> "http://axschema.org/namePerson/first",
        "lastname" -> "http://axschema.org/namePerson/last"
      )
    ).map(Redirect(_)).recover {
      case t: Throwable => {
        println(t.getMessage())
        Redirect(routes.Application.index())
      }
    }
  }

  def createUser(firstName:String, lastName:String, email:String):User = {
    val id = java.util.UUID.randomUUID()
    User(
      identifier = UserId(id.toString),
      name = lastName + firstName ,
      email = email
    )
  }

  def openIDCallback = Action.async { implicit request =>
    OpenID.verifiedId.map(info => {
      val firstName = info.attributes("firstname")
      val lastName = info.attributes("lastname")
      val email = info.attributes("email")
      println(email)
      val repository = UserRepository.ofJDBC()
      implicit val ctx = SyncEntityIOContextOnJDBC.autoSessionContext
      val user = repository.resolveByEmail(info.attributes("email")).recoverWith {
        case t:EntityNotFoundException =>
          repository.store(createUser(firstName, lastName ,email)).map(_.entity)
      }
      user.get
      Redirect(routes.Application.index()).withSession(
        "openId" -> info.id,
        "firstName" -> info.attributes("firstname"),
        "lastName" -> info.attributes("lastname"),
        "email" -> email)
    }).recover {
      case t: Throwable => {
        println(t.getMessage())
        Redirect("http://www.google.co.jp/")
      }
    }
  }

}