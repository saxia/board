package controllers

import play.api.mvc._

trait Auth {
  def mail(request: RequestHeader): Option[String] = request.session.get("email")

  def onUnauthorized(request: RequestHeader): Result = {
    Results.Redirect(routes.Application.login())
  }

  def withAuth(f: => String => Request[AnyContent] => Result) = {
    Security.Authenticated(mail, onUnauthorized) { user =>
      Action(request =>
        f(user)(request)
      )
    }
  }
}