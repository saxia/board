package controllers

import play.api._
import play.api.mvc._

object Application extends Controller with Auth {

  def index = withAuth { mail => implicit request =>
    Redirect("/boards")
  }

  def login = Action { implicit request =>
    Ok(views.html.login())
  }

  def logout = Action { implicit request =>
    Redirect(routes.Application.login).withNewSession
  }

}