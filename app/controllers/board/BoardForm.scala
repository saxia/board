package controllers.board

import play.api.data._
import play.api.data.Forms._

object BoardForm{
  case class FormData (title:String)

  val form = Form(mapping(
    "title" -> nonEmptyText
  )(FormData.apply)(FormData.unapply))

}



