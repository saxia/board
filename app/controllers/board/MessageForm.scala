package controllers.board

import play.api.data.Form
import play.api.data.Forms._

object MessageForm{
  case class FormData (title:String, body:String)

  val form = Form(mapping(
    "title" -> nonEmptyText,
    "body" -> nonEmptyText
  )(FormData.apply)(FormData.unapply))

}
