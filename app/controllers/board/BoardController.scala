package controllers.board

import controllers.Application._
import controllers.Auth
import controllers.service.messageView.MessageViewService
import domain.lifecycle.SyncEntityIOContextOnJDBC
import domain.lifecycle.board.board.BoardRepository
import domain.lifecycle.board.message.MessageRepository
import domain.lifecycle.user.UserRepository
import domain.model.board.board.BoardId
import domain.model.user.AdminUser
import play.mvc._

object BoardController extends Controller with Auth {

  /**
   * 存在する掲示板のリストを表示します。
   */
  def getBoardList() = withAuth { mail => implicit request =>
    implicit val ctx = SyncEntityIOContextOnJDBC.autoSessionContext
    BoardRepository.ofJDBC().resolveAll().map(boards =>
      Ok(views.html.boadList(boards))
    ).get
  }

  /**
   * 掲示板を作成します。
   * @return
   */
  def createBoard() = withAuth { mail => implicit request =>
    val form = BoardForm.form.bindFromRequest()
    if (form.hasErrors) {
      form.errors.foreach(e => println(e.message))
      Redirect(routes.BoardController.getBoardList())
    } else {
      implicit val ctx = SyncEntityIOContextOnJDBC.autoSessionContext
      val boardTitle = form.get.title
      val user = UserRepository.ofJDBC().resolveByEmail(mail)
      // Admin昇格については保留
      val admin = user.map(AdminUser(_, ""))
      admin.map(admin => BoardRepository.ofJDBC().store(admin.createBoard(boardTitle)))
      Redirect(routes.BoardController.getBoardList())
    }
  }

  /**
   * 掲示板の投稿を取得します。
   */
  def getBoard(board_id: String) = withAuth { mail => implicit request =>
    implicit val ctx = SyncEntityIOContextOnJDBC.autoSessionContext
    val boardId = BoardId(board_id)
    val messages = MessageRepository.ofJDBC().resolveByBoardId(boardId)
    val action = for {
      messages <- MessageViewService(boardId, messages).createMessageViews()
      board <- BoardRepository.ofJDBC().resolveBy(boardId)
    } yield {
      Ok(views.html.board(board, messages))
    }
    action.get
  }

  /**
   * ボードにメッセージを投稿します。
   */
  def postBoard(board_id: String) = withAuth { mail => implicit request =>
    implicit val ctx = SyncEntityIOContextOnJDBC.autoSessionContext
    val form = MessageForm.form.bindFromRequest()
    if (form.hasErrors) {
      form.errors.foreach(e => println(e.message))
      Redirect(routes.BoardController.getBoard(board_id))
    }
    else {
      val user = UserRepository.ofJDBC().resolveByEmail(mail)
      user.map(_.createMessage(BoardId(board_id), form.get.title, form.get.body)).foreach(
        MessageRepository.ofJDBC().store
      )
      Redirect(routes.BoardController.getBoard(board_id))
    }
  }

  def deleteBoard(board_id: String) = TODO

}
