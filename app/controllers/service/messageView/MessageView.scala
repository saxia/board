package controllers.service.messageView

import org.joda.time.DateTime

case class MessageView(
                        userName:String,
                        email:String,
                        title:String,
                        body:String,
                        postTime:DateTime
                        )
