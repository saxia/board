package controllers.service.messageView

import domain.lifecycle.user.UserRepository
import domain.model.board.board.BoardId
import domain.model.board.message.Message
import domain.model.user.User
import org.sisioh.dddbase.core.lifecycle.sync.SyncEntityIOContext

import scala.util.Try

case class MessageViewService(
                             boardId:BoardId,
                             messages:Try[Seq[Message]],
                             userRepository: UserRepository = UserRepository.ofJDBC()
                               )(implicit val ctx:SyncEntityIOContext) {

  def createMessageViews():Try[Seq[MessageView]] = for{
      msg <- messages
      users <- userRepository.resolveByMulti(msg.map(_.userId).distinct:_*)
    } yield msg.map(convertToMessageView(_, users))

  private[this] def convertToMessageView(message: Message, users:Seq[User]) = {
    val user = users.find(_.identifier == message.userId).getOrElse(throw new Exception("user not found"))
    MessageView(
      userName = user.name,
      email =  user.email,
      title = message.title,
      body = message.body,
      postTime = message.postTime
    )
  }
}
