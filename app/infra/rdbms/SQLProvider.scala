package infra.rdbms

import scalikejdbc.{ SQL, NoExtractor }

trait SQLProvider[ID] {

  def select(id: ID): SQL[Nothing, NoExtractor]

  def exist(id: ID): SQL[Nothing, NoExtractor]

  def insert(entity: Map[String, Any]): SQL[Nothing, NoExtractor]

  def update(entity: Map[String, Any]): SQL[Nothing, NoExtractor]

  def delete(id: ID): SQL[Nothing, NoExtractor]

  def truncate(option: Option[String] = None): SQL[Nothing, NoExtractor]

  def toBackQuoteString(self: String): String = "`" + self.replace("`", "``") + "`"
}