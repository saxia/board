package infra.rdbms

import scalikejdbc._

import scalikejdbc.{NoExtractor, SQL}

case class BoardSQLProvider() extends SQLProvider[String] {
  override def select(id: String): SQL[Nothing, NoExtractor] =
    sql"SELECT * FROM `board` WHERE `id` = /*'id*/'id'".bindByName('id -> id)

  override def exist(id: String): SQL[Nothing, NoExtractor] =
    sql"SELECT COUNT(*) FROM `board` WHERE `id` = /*'id*/'id'".bindByName('id -> id)


  override def update(entity: Map[String, Any]): SQL[Nothing, NoExtractor] =
    sql"""
         UPDATE `board`
         SET
         user_id = /*'user_id*/'user_id',
         title = /*'title*/,
         version = version + 1
         WHERE
         id = /*'id*/'id'
         AND
         version = /*'version*/1
             """.bindByName(
        'id -> entity("id"),
        'user_id -> entity("user_id"),
        'title -> entity("title"),
        'version -> entity("version")
      )

  override def insert(entity: Map[String, Any]): SQL[Nothing, NoExtractor] =
    sql"""
          INSERT INTO `board`(`id`, `user_id`,`title`,`version`)
          VALUES(
          /*'id*/'id',
          /*'user_id*/'user_id',
          /*'title*/'title',
          1
          )
       """.bindByName(
        'id -> entity("id"),
        'user_id -> entity("user_id"),
        'title -> entity("title")
      )

  override def truncate(option: Option[String]): SQL[Nothing, NoExtractor] = ???

  override def delete(id: String): SQL[Nothing, NoExtractor] =
    sql"DELETE FROM `board` WHERE `id` = /*'id*/'id'".bindByName('id -> id)

  def selectTop(limit:Int) =
    sql"SELECT * FROM `board` LIMIT /*'limit*/1".bindByName('limit -> limit)
}
