package infra.rdbms

import scalikejdbc._

case class UserSQLProvider() extends SQLProvider[String] {
  def select(id: String): SQL[Nothing, NoExtractor] =
    sql"SELECT * FROM `user` WHERE `id` = /*'id*/'id'".bindByName('id -> id)

  def exist(id: String): SQL[Nothing, NoExtractor] =
    sql"SELECT COUNT(*) FROM `user` WHERE `id` = /*'id*/'id'".bindByName('id -> id)

  def selectByEmail(email:String): SQL[Nothing, NoExtractor] =
    sql"SELECT * FROM `user` WHERE `email` = /*'email*/'email'".bindByName('email -> email)

  def update(entity: Map[String, Any]): SQL[Nothing, NoExtractor] =
    sql"""
         UPDATE `user`
         SET
          `name` = /*'name*/'name',
          `email` = /*'email*/'email',
          `version` = `version` + 1
         WHERE
          `id` = /*'id*/'id'
          AND
          `version` = /*'version*/1
       """.bindByName(
        'id -> entity("id"),
        'name -> entity("name"),
        'email -> entity("email"),
        'version -> entity("version")
      )

  def insert(entity: Map[String, Any]): SQL[Nothing, NoExtractor] =
    SQL("""
         INSERT INTO `user`
          (`id`, `name`, `email`, `version`)
         VALUES (
         /*'id*/'id',
         /*'name*/'name',
         /*'email*/'email',
         1)
     """).bindByName(
        'id -> entity("id"),
        'name -> entity("name"),
        'email -> entity("email")
      )

  def truncate(option: Option[String]): SQL[Nothing, NoExtractor] = ???

  def delete(id: String): SQL[Nothing, NoExtractor] =
    sql"DELETE FROM `user` WHERE `id` = /*'id*/'id'".bindByName('id -> id)
}
