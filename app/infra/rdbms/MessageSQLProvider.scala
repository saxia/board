package infra.rdbms

import scalikejdbc._

case class MessageSQLProvider() extends SQLProvider[String]{

  def select(id: String): SQL[Nothing, NoExtractor] =
    sql"SELECT * FROM `message` WHERE `id` = /*'id*/'id'".bindByName('id -> id)

  def exist(id: String): SQL[Nothing, NoExtractor] =
    sql"SELECT COUNT(*) FROM `message` WHERE `id` = /*'id*/'id'".bindByName('id -> id)

  def selectByBoardId(boardId:String):  SQL[Nothing, NoExtractor] =
    sql"SELECT * FROM `message` WHERE `board_id` = /*'board_id*/'id'".bindByName('board_id -> boardId)


  override def insert(entity: Map[String, Any]): SQL[Nothing, NoExtractor] =
    sql"""
          INSERT INTO `message`(`id`, `user_id`,`board_id`,`title`,`body`,`deleted_time`, `modify_time`, `post_time`, `version`)
          VALUES(
          /*'id*/'id',
          /*'user_id*/'user_id',
          /*'board_id*/'board_id',
          /*'title*/'title',
          /*'body*/'body',
          /*'deleted_time*/'deleted_time',
          /*'modify_time*/'modify_time',
          /*'post_time*/'post_time',
          1
          )
       """.bindByName(
        'id -> entity("id"),
        'user_id -> entity("user_id"),
        'board_id -> entity("board_id"),
        'title -> entity("title"),
        'body -> entity("body"),
        'deleted_time -> entity("deleted_time"),
        'modify_time -> entity("modify_time"),
        'post_time -> entity("post_time")
      )

  override def update(entity: Map[String, Any]): SQL[Nothing, NoExtractor] =
    sql"""
         UPDATE `message`
         SET
         `user_id` = /*'user_id*/'user_id',
         `board_id` = /*'board_id*/'board_id',
         `title` = /*'title*/'title',
         `body` = /*'body*/'body',
         `deleted_time` = /*'deleted_time*/'deleted_time',
         `modify_time` = /*'modify_time*/'modify_time',
         `post_time` = /*'post_time*/'post_time',
         `version = version + 1
         WHERE
         id = /*'id*/'id'
         AND
         version = /*'version*/1
             """.bindByName(
        'id -> entity("id"),
        'user_id -> entity("user_id"),
        'board_id -> entity("board_id"),
        'title -> entity("title"),
        'body -> entity("body"),
        'deleted_time -> entity("deleted_time"),
        'modify_time -> entity("modify_time"),
        'post_time -> entity("post_time"),
        'version -> entity("version")
      )

  override def truncate(option: Option[String]): SQL[Nothing, NoExtractor] = ???

  override def delete(id: String): SQL[Nothing, NoExtractor] =
    sql"SELECT COUNT(*) FROM `message` WHERE `id` = /*'id*/'id'".bindByName('id -> id)
}
