import play.api._
import scalikejdbc.config.DBs

object Global extends GlobalSettings {

  override def onStart(application: Application): Unit = {
    DBs.setupAll()
    super.onStart(application)
  }

  override def onStop(application: Application): Unit = {
    DBs.closeAll()
    super.onStop(application)
  }
}

