package domain.model.user

import domain.model.board.board.{BoardId, Board}
import domain.model.board.message.Message
import org.joda.time.DateTime

private[domain] case class AdminUserImpl(user: User,
                         password: String
                          ) extends AdminUser {
  val name: String = user.name
  val email: String = user.email
  val version: Option[Int] = user.version
  val identifier: UserId = user.identifier

  def deleteMessage(base: Message): Message = base.withDeleted()

  def createBoard(title:String): Board = {
    val id = java.util.UUID.randomUUID()
    Board(
      BoardId(id.toString),
      this.identifier,
      title,
      0,
      DateTime.now
    )
  }

  def modifyMessage(base: Message, title: String, body: String): Message = user.modifyMessage(base, title, body)

  def createMessage(boardId: BoardId,title: String, body: String): Message = user.createMessage(boardId,title,body)
}
