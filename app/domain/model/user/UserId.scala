package domain.model.user

import org.sisioh.dddbase.core.model.Identifier

case class UserId( value: String ) extends Identifier[String]
