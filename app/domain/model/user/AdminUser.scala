package domain.model.user

import domain.model.board.board.Board
import domain.model.board.message.Message

trait AdminUser extends User {
  val password:String

  def createBoard(title:String):Board

  def deleteMessage(base:Message):Message
}

object AdminUser {
  def apply(user:User, password:String):AdminUser = AdminUserImpl(user, password)
}