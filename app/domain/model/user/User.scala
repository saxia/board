package domain.model.user

import domain.model.BaseEntity
import domain.model.board.board.BoardId
import domain.model.board.message.Message

trait User extends BaseEntity[UserId]{
  val name:String
  val email:String

  def createMessage(boardId: BoardId,title:String, body:String):Message
  def modifyMessage(base:Message,title:String, body:String):Message
}


object User{
  def apply(
             identifier:UserId,
             name:String,
             email:String,
             version:Option[Int] = None
             ):User = UserImpl(
    identifier,
    name,
    email,
    version
  )
}