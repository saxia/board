package domain.model.user

import domain.model.board.board.BoardId
import domain.model.board.message.{MessageId, Message}
import org.joda.time.DateTime

private[domain] case class UserImpl(
                   identifier:UserId,
                   name:String,
                   email:String,
                   version:Option[Int]
                     ) extends User{

  def createMessage(boardId: BoardId ,title:String, body:String): Message = {
    val id = java.util.UUID.randomUUID()
    Message(
      MessageId(id.toString),
      this.identifier,
      boardId,
      title,
      body,
      DateTime.now
    )
  }

  def modifyMessage(base: Message, title: String, body: String): Message =
    base.withTitleAndBody(title, body)
}
