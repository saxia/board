package domain.model

import org.sisioh.dddbase.core.model.{Identifier, Entity}

trait BaseEntity[ID <: Identifier[_]] extends Entity[ID] {
  val version: Option[Int]
}
