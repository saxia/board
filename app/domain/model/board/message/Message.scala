package domain.model.board.message

import domain.model.BaseEntity
import domain.model.board.board.BoardId
import domain.model.user.UserId
import org.joda.time.DateTime

trait Message extends BaseEntity[MessageId]{
  val userId:UserId
  val boardId: BoardId
  val title:String
  val body:String
  val deletedTime:Option[DateTime]
  val modifyTime:Option[DateTime]
  val postTime:DateTime

  private[domain] def withDeleted():Message

  private[domain] def withTitleAndBody(title:String , body:String):Message
}

object Message{
  def apply(
           identifier:MessageId,
           userId:UserId,
           boardId: BoardId,
           title:String,
           body:String,
           postDate: DateTime,
           modifyDate: Option[DateTime] = None,
           deletedTime: Option[DateTime] = None,
           version: Option[Int] = None
             ):Message = MessageImpl(
    identifier,
    userId,
    boardId,
    title,
    body,
    postDate,
    modifyDate,
    deletedTime,
    version
  )
}