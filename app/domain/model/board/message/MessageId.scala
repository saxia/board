package domain.model.board.message

import org.sisioh.dddbase.core.model.Identifier

case class MessageId (value:String) extends Identifier[String]
