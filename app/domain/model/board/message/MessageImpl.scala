package domain.model.board.message

import domain.model.board.board.BoardId
import domain.model.user.UserId
import org.joda.time.DateTime

case class MessageImpl(
                        identifier: MessageId,
                        userId: UserId,
                        boardId: BoardId,
                        title: String,
                        body: String,
                        postTime: DateTime,
                        modifyTime: Option[DateTime],
                        deletedTime: Option[DateTime],
                        version: Option[Int]
                        )  extends Message{

  private[domain] def withDeleted(): Message =
    copy(deletedTime = Some(DateTime.now))

  private[domain] def withTitleAndBody(title: String, body: String): Message =
    copy(title = title, body = body, modifyTime = Some(DateTime.now))

}
