package domain.model.board.board

import domain.model.user.UserId
import org.joda.time.DateTime

private[board] case class BoardImpl(
                                     identifier: BoardId,
                                     userId: UserId,
                                     title: String,
                                     postCount: Long,
                                     lastPost: DateTime,
                                     version: Option[Int]
                                     ) extends Board {
}
