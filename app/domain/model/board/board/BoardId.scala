package domain.model.board.board

import org.sisioh.dddbase.core.model.Identifier

case class BoardId (value:String) extends Identifier[String]
