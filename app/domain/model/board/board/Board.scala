package domain.model.board.board

import domain.model.BaseEntity
import domain.model.user.UserId
import org.joda.time.DateTime

trait Board extends BaseEntity[BoardId] {
  val userId: UserId
  val title: String

  val postCount: Long
  val lastPost: DateTime
}

object Board {
  def apply(
             identifier: BoardId,
             userId: UserId,
             title: String,
             postCount: Long,
             lastPost: DateTime,
             version: Option[Int] = None
             ): Board = BoardImpl(
    identifier,
    userId,
    title,
    postCount,
    lastPost,
    version
  )
}