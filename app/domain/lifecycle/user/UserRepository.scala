package domain.lifecycle.user

import domain.lifecycle.user.jdbc.UserRepositoryOnJDBC
import domain.model.user.{User, UserId}
import org.sisioh.dddbase.core.lifecycle.sync.SyncRepository

import scala.util.Try

trait UserRepository extends SyncRepository[UserId, User]{
  def resolveByEmail(email:String)(implicit ctx:Ctx):Try[User]
}

object UserRepository {
  def ofJDBC():UserRepository = new UserRepositoryOnJDBC()
}