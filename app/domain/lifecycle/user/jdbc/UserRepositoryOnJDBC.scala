package domain.lifecycle.user.jdbc

import domain.lifecycle.AbstractRepositoryOnJDBC
import domain.lifecycle.user.UserRepository
import domain.model.user.{UserId, User}
import infra.rdbms.{UserSQLProvider, SQLProvider}
import org.sisioh.dddbase.core.lifecycle.EntityNotFoundException
import scalikejdbc.WrappedResultSet

import scala.util.Try

class UserRepositoryOnJDBC extends AbstractRepositoryOnJDBC[UserId, User] with UserRepository{
  override protected val modelName: String = "UserRepositoryOnJDBC"

  override protected def convertToEntity(resultSet: WrappedResultSet): User = User(
    identifier = UserId(resultSet.string("id")),
    name = resultSet.string("name"),
    email = resultSet.string("email"),
    version = resultSet.intOpt("version")
  )

  override protected val sqlProvider= UserSQLProvider()

  override protected def convertToMap(entity: User): Map[String, Any] = Map(
    "id" -> entity.identifier.value,
    "name" -> entity.name,
    "email" -> entity.email,
    "version" -> entity.version
  )

  override def resolveByEmail(email: String)(implicit ctx: Ctx): Try[User] = Try {
    implicit val dbSession = getDBSession(ctx)
    sqlProvider.selectByEmail(email).map(convertToEntity).single().apply().
      getOrElse(throw new EntityNotFoundException(Some(s"modelName = $modelName, email = $email")))
  }

  override type This = UserRepositoryOnJDBC
}
