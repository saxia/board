package domain.lifecycle

import org.sisioh.dddbase.core.lifecycle.sync.SyncEntityIOContext
import scalikejdbc.{DBSession, NamedAutoSession, NamedDB}

case class SyncEntityIOContextOnJDBC(session: DBSession) extends SyncEntityIOContext

object SyncEntityIOContextOnJDBC {

  def namedAutoSessionContext(dbName:Symbol): SyncEntityIOContextOnJDBC =
    SyncEntityIOContextOnJDBC(NamedAutoSession(dbName))

  def autoSessionContext: SyncEntityIOContextOnJDBC = namedAutoSessionContext('default)

  def localTransaction[A](dbName: Symbol)(execution: SyncEntityIOContextOnJDBC => A): A = {
    NamedDB(dbName).localTx { implicit session =>
      val ctx = SyncEntityIOContextOnJDBC(session)
      execution(ctx)
    }
  }

  def localTransaction[A](execution: SyncEntityIOContextOnJDBC => A): A = {
    localTransaction('default)(execution)
  }
}