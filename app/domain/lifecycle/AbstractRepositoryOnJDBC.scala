package domain.lifecycle

import domain.model.BaseEntity
import infra.rdbms.SQLProvider
import org.sisioh.dddbase.core.lifecycle.sync.{ SyncRepository, SyncResultWithEntity }
import org.sisioh.dddbase.core.lifecycle.{ EntityIOContext, EntityNotFoundException, RepositoryException }
import org.sisioh.dddbase.core.model.{Entity, Identifier}
import scalikejdbc.{ DBSession, WrappedResultSet }

import scala.util.Try

abstract class AbstractRepositoryOnJDBC[ID <: Identifier[_], E <: BaseEntity[ID]] extends SyncRepository[ID, E] {

  protected val modelName: String

  protected val sqlProvider: SQLProvider[String]

  private implicit def toIdString(identifier: ID): String = identifier.value.toString

  protected def convertToEntity(resultSet: WrappedResultSet): E

  protected def convertToMap(entity: E): Map[String, Any]

  protected def getDBSession(ctx: EntityIOContext[Try]): DBSession = {
    ctx match {
      case SyncEntityIOContextOnJDBC(session) => session
      case _ =>
        throw new IllegalArgumentException(s"$ctx is type miss match. please set to SyncEntityIOContextOnDB.")
    }
  }

  override def store(entity: E)(implicit ctx: Ctx): Try[Result] = Try {
    implicit val dbSession = getDBSession(ctx)
    val result = entity.version.map { _ =>
      sqlProvider.update(convertToMap(entity)).update().apply()
    }.getOrElse {
      sqlProvider.insert(convertToMap(entity)).update().apply()
    }
    if (result > 0) {
      SyncResultWithEntity[This, ID, E](this.asInstanceOf[This], entity)
    } else {
      throw new RepositoryException(Some(s"modelName = $modelName, entity = $entity"))
    }
  }

  override def deleteBy(identifier: ID)(implicit ctx: Ctx): Try[Result] = {
    resolveBy(identifier).map { entity =>
      implicit val dbSession = getDBSession(ctx)
      val result = sqlProvider.delete(identifier).update().apply()
      if (result == 1) {
        SyncResultWithEntity[This, ID, E](this.asInstanceOf[This], entity)
      } else {
        throw new RepositoryException(Some(s"modelName = $modelName, entity = $entity"))
      }
    }
  }

  override def resolveBy(identifier: ID)(implicit ctx: Ctx): Try[E] = Try {
    implicit val dbSession = getDBSession(ctx)
    sqlProvider.select(identifier).map(convertToEntity).single().apply().
      getOrElse(throw new EntityNotFoundException(Some(s"modelName = $modelName, identifier = $identifier")))
  }

  override def existBy(identifier: ID)(implicit ctx: Ctx): Try[Boolean] = Try {
    implicit val dbSession = getDBSession(ctx)
    sqlProvider.exist(identifier).map(_.int(1) == 1).single().apply().
      getOrElse(throw new RepositoryException(Some(s"modelName = $modelName, identifier = $identifier")))
  }
}