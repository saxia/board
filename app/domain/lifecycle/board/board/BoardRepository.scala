package domain.lifecycle.board.board

import domain.lifecycle.board.board.jdbc.BoardRepositoryOnJDBC
import domain.model.board.board.{Board, BoardId}
import domain.model.user.UserId
import org.sisioh.dddbase.core.lifecycle.sync.SyncRepository

import scala.util.Try

trait BoardRepository extends SyncRepository[BoardId, Board] {
  def resolveAll(limit:Int = 1000)(implicit ctx: Ctx):Try[Seq[Board]]
  def resolveByUserId(userId: UserId)(implicit ctx: Ctx): Try[Seq[Board]]
}

object BoardRepository {
  def ofJDBC():BoardRepository = new BoardRepositoryOnJDBC()

}