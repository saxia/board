package domain.lifecycle.board.board.jdbc

import infra.rdbms.BoardSQLProvider
import domain.lifecycle.AbstractRepositoryOnJDBC
import domain.lifecycle.board.board.BoardRepository
import domain.model.board.board.{Board, BoardId}
import domain.model.user.UserId
import org.joda.time.DateTime
import scalikejdbc.WrappedResultSet

import scala.util.Try

class BoardRepositoryOnJDBC extends AbstractRepositoryOnJDBC[BoardId, Board] with BoardRepository{
  override protected val modelName: String = "BoardRepositoryOnJDBC"

  override protected def convertToEntity(resultSet: WrappedResultSet): Board = Board(
    identifier = BoardId(resultSet.string("id")),
    userId = UserId(resultSet.string("user_id")),
    title = resultSet.string("title"),
    version = resultSet.intOpt("version"),
    postCount = 0,
    lastPost = new DateTime(2000,1,1,12,0)
  )

  override protected def convertToMap(entity: Board): Map[String, Any] = Map(
    "id" -> entity.identifier.value,
    "user_id" -> entity.userId.value,
    "title" -> entity.title
  )

  override protected val sqlProvider = BoardSQLProvider()

  override def resolveByUserId(userId: UserId)(implicit ctx: Ctx): Try[Seq[Board]] = ???

  override def resolveAll(limit: Int)(implicit ctx: Ctx): Try[Seq[Board]] = Try {
    implicit val dbSession = getDBSession(ctx)
    sqlProvider.selectTop(limit).map(convertToEntity).toList().apply().toSeq
  }

  override type This = BoardRepositoryOnJDBC
}
