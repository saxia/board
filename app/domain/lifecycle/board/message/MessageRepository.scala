package domain.lifecycle.board.message

import domain.lifecycle.board.message.jdbc.MessageRepositoryOnJDBC
import domain.model.board.board.BoardId
import domain.model.board.message.{Message, MessageId}
import org.sisioh.dddbase.core.lifecycle.sync.SyncRepository

import scala.util.Try

trait MessageRepository extends SyncRepository[MessageId, Message]{
  def resolveByBoardId(boardId:BoardId)(implicit ctx:Ctx):Try[Seq[Message]]
}

object MessageRepository {
  def ofJDBC():MessageRepository = {
    new MessageRepositoryOnJDBC()
  }
}
