package domain.lifecycle.board.message.jdbc

import domain.lifecycle.AbstractRepositoryOnJDBC
import domain.lifecycle.board.message.MessageRepository
import domain.model.board.board.BoardId
import domain.model.board.message.{MessageId, Message}
import domain.model.user.UserId
import infra.rdbms.{MessageSQLProvider, SQLProvider}
import scalikejdbc.WrappedResultSet

import scala.util.Try

class MessageRepositoryOnJDBC extends AbstractRepositoryOnJDBC[MessageId, Message] with MessageRepository {
  override protected val modelName: String = "MessageRepositoryOnJDBC"

  override protected def convertToEntity(resultSet: WrappedResultSet): Message = Message(
    identifier = MessageId(resultSet.string("id")),
    boardId = BoardId(resultSet.string("board_id")),
    userId = UserId(resultSet.string("user_id")),
    title = resultSet.string("title"),
    body = resultSet.string("body"),
    deletedTime = resultSet.jodaDateTimeOpt("deleted_time"),
    modifyDate = resultSet.jodaDateTimeOpt("modify_time"),
    postDate = resultSet.jodaDateTime("post_time"),
    version = resultSet.intOpt("version")
  )

  override protected def convertToMap(entity: Message): Map[String, Any] = Map(
    "id" -> entity.identifier.value,
    "user_id" -> entity.userId.value,
    "board_id" -> entity.boardId.value,
    "title" -> entity.title,
    "body" -> entity.body,
    "deleted_time" -> entity.deletedTime,
    "modify_time" -> entity.modifyTime,
    "post_time" -> entity.postTime,
    "version" -> entity.version
  )

  override protected val sqlProvider = MessageSQLProvider()

  override def resolveByBoardId(boardId: BoardId)(implicit ctx: Ctx): Try[Seq[Message]] = Try {
    implicit val dbSession = getDBSession(ctx)
    sqlProvider.selectByBoardId(boardId.value).map(convertToEntity).toList().apply().toSeq
  }

  override type This = MessageRepositoryOnJDBC
}
