CREATE TABLE `user` (
  `id`      VARCHAR(255) NOT NULL,
  `name`    VARCHAR(255) NOT NULL,
  `email`   VARCHAR(255),
  `version` INT
)
  ENGINE=InnoDB
  CHARACTER SET utf8;