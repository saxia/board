CREATE TABLE `message` (
  `id`      VARCHAR(255) NOT NULL,
  `user_id` VARCHAR(255) NOT NULL,
  `board_id` VARCHAR(255) NOT NULL,
  `title`   VARCHAR(255) NOT NULL,
  `body`   TEXT NOT NULL,
  `deleted_time` DATETIME,
  `modify_time` DATETIME,
  `post_time` DATETIME NOT NULL ,
  `version` INT
)
  ENGINE=InnoDB
  CHARACTER SET utf8;