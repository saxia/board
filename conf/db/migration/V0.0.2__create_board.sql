CREATE TABLE `board` (
  `id`      VARCHAR(255) NOT NULL,
  `user_id` VARCHAR(255) NOT NULL,
  `title`   VARCHAR(255) NOT NULL,
  `version` INT
)
  ENGINE=InnoDB
  CHARACTER SET utf8;