package SampleSpec

import domain.model.board.board.Board
import org.specs2.mutable.Specification

class SampleSpec extends Specification {

  def fibonat(x: Int): Int = {
    if (x <= 0) {
      throw new IllegalArgumentException("x must be greater than 0")
    }
    x match {
      case 1 => 1
      case 2 => 2
      case _ => fibonat(x - 2) + fibonat(x - 1)
    }
  }

  def fibSeq(x:Int):Seq[Int] = {
    x match {
      case 1 => Seq(fibonat(1))
      case _ => fibSeq(x - 1) union Seq(fibonat(x))
    }
  }

  def findt(x:Seq[String]): Seq[String] = ???


  "fib" in {
    "1" in {
      fibonat(1) must beEqualTo(1)
    }
    "2" in {
      fibonat(2) must beEqualTo(2)
    }
    "3" in {
      fibonat(3) must beEqualTo(3)
    }
    "4" in {
      fibonat(4) must beEqualTo(5)
    }
  }
}
